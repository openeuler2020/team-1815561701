# xthread

## 介绍
xthread(X Thread)是一个用户态的多线程库，调度器位于用户态，不是依赖于内核的调度器。
## 来源
本项目是参加2020 openEuler高校开发者大赛赛题4-LUTF - Linux Userspace Task Framework的作品。
## 赛题要求
LUTF要完成：
任务的定义与抽象，典型的，一个任务可以是一个C语言函数；  
提供任务的管理接口，提供任务的创建，管理等功能；  
进行任务的调度；  
约束：  
纯 Linux 用户态实现；  
同时支持的任务数量不少于一百万；  
任务的调度支持 FIFO；  
基于 setjmp 和 longjmp 实现任务切换；  
基于 signal 实现中断；  
## 完成概况
按照赛题要求完成全部要求！  
导师说可以使用非setjmp和longjmp实现任务切换，于是选择了更简单的ucontext来实现。
## 使用方法
xthread的编程接口和pthread编程接口相似，使得有过pthread使用经验的开发者可以快速通过[开发文档](/doc/docmain.md)上手开发。
## 编译方法
```shell
make # 编译库并且生成demo
```
## 编程案例
见demo目录  
test_cancel:    测试取消线程功呢  
test_coroutine: 测试协程功能  
test_factor:    测试线程的生产  
test_fifo:      基于信号量和互斥锁的fifo队列  
test_thread:    测试线程功能  
test_mutex:     测试互斥锁  
test_sema:      测试信号量  
test_timer:     测试定时器  
test_yield:     测试yield功能  

执行demo：  
./demo/demo+测试功能名,不加测试功能名默认测试thread   
例如：  
./demo/demo fifo  
./demo/demo yield  

## api详解
[docmain](/doc/docmain.md):        主文档  
[thread](/doc/thread.md):         xthread核心使用帮助  
[thread-attr](/doc/thread-attr.md):    xthread属性使用帮助  
[thread-lock](/doc/thread-lock.md):    自旋锁使用帮助  
[thread-mutex](/doc/thread-mutex.md):   互斥锁使用帮助  
[thread-sema](/doc/thread-sema.md):    信号量使用帮助  
[thread-timer](/doc/thread-timer.md):   定时器使用帮助  
## 注意！！！
1.xthread基于SIGALRM信号，因此你使用后不能用这个信号。  
2.如果使用的是共享库，则需要把共享库地址添加到环境变量中，才能执行demo：	export LD_LIBRARY_PATH=./xthread/:$LD_LIBRARY_PATH  
