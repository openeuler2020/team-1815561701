#ifndef _DEMO_TEST_H
#define _DEMO_TEST_H

#include <xthread.h>

// ----
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int test_timer(int argc, char *argv[]);
int test_cancel(int argc, char *argv[]);
int test_factor(int argc, char *argv[]);
int test_mutex(int argc, char *argv[]);
int test_sema(int argc, char *argv[]);
int test_thread(int argc, char *argv[]);
int test_fifo(int argc, char *argv[]);
int test_yield(int argc, char *argv[]);
int test_coroutine(int argc, char *argv[]);

#endif /* _DEMO_TEST_H */