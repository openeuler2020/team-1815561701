#include "test.h"

void *cancel_thread(void *arg)
{
    printf("cancel thread start\n");
    xthread_setcancelstate(XTHREAD_CANCEL_DISABLE, NULL);
    xthread_setcanceltype(XTHREAD_CANCEL_ASYCHRONOUS, NULL);
    while (1) {
        xthread_testcancel();
        printf("cancel thread sleep 2s\n");
        xthread_sleep(2);
        printf("cancel thread sleep 2s done\n");
    }
    return (void *) 0x1234abcd;
}

int test_cancel(int argc, char *argv[])
{
    xthread_t tid;
    if (xthread_create(&tid, NULL, cancel_thread, NULL) < 0)
            printf("xthread create failed!\n");
    printf("master sleep 2s\n");
    xthread_sleep(2);
    printf("master sleep 2s done\n");
    
    if (xthread_cancel(tid)) {
            printf("master do cancel failed\n");
    }
    void *status;
    xthread_join(tid, &status);
    printf("cancel thread %x exit with %x\n", tid, status);
    
    return 0;
}
