#include "test.h"

typedef int (*main_t) (int , char **);
typedef struct {
    char *name;
    main_t func;
} testfunc_t;

testfunc_t test_table[] = {
    {"thread", test_thread},
    {"factor", test_factor},
    {"fifo", test_fifo},
    {"mutex", test_mutex},
    {"sema", test_sema},
    {"timer", test_timer},
    {"cancel", test_cancel},
    {"yield", test_yield},
    {"coroutine", test_coroutine},
};

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof(arr[0]))

int main(int argc, char *argv[])
{
    printf("xthread testing...\n");
    unsigned long tcount = xthread_best_count();
    printf("xthread best count:%d\n", tcount);

    int retval = -1;
    if (argc == 1) {
        retval = test_table[0].func(argc, argv);  
        printf("\ntest %s demo done.\n", test_table[0].name);
    } else if (argc == 2) {
        char *p = argv[1];
        int i;
        for (i = 0; i < ARRAY_SIZE(test_table); i++)
            if (!strcmp(p, test_table[i].name)) {
                retval = test_table[i].func(argc, argv);
                printf("\ntest %s demo done.\n", test_table[0].name);
                break;
            }
        if (i >= ARRAY_SIZE(test_table))
            printf("test %s demo not found!\n", p);
    }
    return retval;
}
