#include "test.h"

#define THREAD_NR   100000

void *yield_thread(void *arg)
{
    int i = 3;
    while (i > 0) {
        printf("thread %x,%d: yield start.\n", (int *) arg, xthread_self());
        xthread_yield();
        printf("thread %x,%d: yield end.\n", (int *) arg, xthread_self());    
        i--;
    }
    return NULL;
}

int test_yield(int argc, char *argv[])
{
    xthread_t tid;
    int i;
    for (i = 0; i < THREAD_NR; i++) {
        if (xthread_create(&tid, NULL, yield_thread, (void *) ((unsigned long)i)) < 0)
            printf("xthread create failed!\n");
    }
    void *status;
    xthread_join(tid, &status);
    return 0;
}
