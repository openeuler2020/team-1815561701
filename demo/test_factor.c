#include "test.h"
 
#define PRINT_DELAY     10000
#define MAX_THREAD_NR   100000

void *goods_thread(void *arg)
{
    printf("goods thread %d start\n", xthread_self());
    int i = 0;
    while (1) {
        i++;
        xthread_yield();
        if (i > 3)
            break;
    }
    printf("goods %d exit now\n", xthread_self());
    return (void *)xthread_self();
}

/* if xthread count is more than max, we need swap thread to fs. */
void *factor_thread(void *arg)
{
    printf("factor starting...\n");
    
    int i = 0;
    xthread_t tid;
    void *status;
    while (1) {
        if (xthread_create(&tid, NULL, goods_thread, NULL) < 0)
                printf("xthread create failed!\n");
        i++;
        if (i > MAX_THREAD_NR)
                break;
    }
    printf("factor exit now.\n");
    return (void *) 0x1234abcd;
}

int test_factor(int argc, char *argv[])
{
    xthread_t tid;
    if (xthread_create(&tid, NULL, factor_thread, NULL) < 0)
            printf("xthread create failed!\n");
    
    void *status;
    xthread_join(tid, &status);
    printf("factor thread %x exit with %x\n", tid, status);
    
    xthread_sleep(5);
    return 0;
}
