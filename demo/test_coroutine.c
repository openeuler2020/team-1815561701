#include "test.h"

#define THREAD_NR   100000

void *coroutine_thread(void *arg)
{
    int i = 3;
    while (i > 0) {
        printf("thread %x,%d: coroutine start.\n", (int *) arg, xthread_self());
        xthread_yield();
        printf("thread %x,%d: coroutine end.\n", (int *) arg, xthread_self());    
        i--;
    }
    return NULL;
}

int test_coroutine(int argc, char *argv[])
{
    /* when disable interrupt, xthread becames coroutine! */
    xthread_disable_interrupt();
    xthread_t tid;
    int i;
    for (i = 0; i < THREAD_NR; i++) {
        if (xthread_create(&tid, NULL, coroutine_thread, (void *) ((unsigned long)i)) < 0)
            printf("xthread create failed!\n");
    }
    void *status;
    xthread_join(tid, &status);
    return 0;
}
