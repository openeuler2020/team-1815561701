#include "test.h"

static void mytimer(void *arg)
{   
    xthread_timer_t *tmr = (xthread_timer_t *) arg;
    printf("5 s timer occur!\n");
    exit(0);
}

static void mytimer2(void *arg)
{
    printf("1 s timer occur!\n");
}

static void *timer_thread(void *arg)
{
    printf("timer thread starting...\n");
    printf("timer sleep 3 seconds.\n");
    xthread_sleep(3);
    printf("timer sleep 3 seconds done.\n");
    
    printf("set 1 seconds timeout timer.\n");
    xthread_timer_t *newtimer = xthread_timer_create(1000, mytimer2, NULL);
    xthread_timer_add(newtimer);
    
    printf("set 5 seconds timeout timer.\n");
    static xthread_timer_t timer;
    xthread_timer_set(&timer, 5000, mytimer, (void *) newtimer);
    xthread_timer_add(&timer);
    
    printf("timer thread end.\n");
    return (void *) 0x1234abcd;
}

int test_timer(int argc, char *argv[])
{
    xthread_t tid;
    if (xthread_create(&tid, NULL, timer_thread, NULL) < 0)
            printf("xthread create failed!\n");
    void *status;
    xthread_join(tid, &status);
    printf("thread %x exit with %x\n", tid, status);
    xthread_sleep(5);
    return 0;
}