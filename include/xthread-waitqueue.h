#ifndef _XTHREAD_WAITQUEUE_H
#define _XTHREAD_WAITQUEUE_H

#include "inneral/list.h"
#include "xthread-spinlock.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	list_t wait_list;	// 记录所有被挂起的进程（等待中）的链表
	xthread_spin_lock_t lock;
} xthread_wait_queue_t;

void xthread_wait_queue_add(xthread_wait_queue_t *wait_queue, void *task_ptr);
void xthread_wait_queue_remove(xthread_wait_queue_t *wait_queue, void *task_ptr);
void xthread_wait_queue_wakeup(xthread_wait_queue_t *wait_queue);
void xthread_wait_queue_wakeup_all(xthread_wait_queue_t *wait_queue);
void xthread_wait_queue_sleep(xthread_wait_queue_t *wait_queue);

static inline void xthread_wait_queue_init(xthread_wait_queue_t *wait_queue)
{
	list_init(&wait_queue->wait_list);
	xthread_spin_init(&wait_queue->lock);
}
static inline int xthread_wait_queue_length(xthread_wait_queue_t *wait_queue)
{
    xthread_spin_lock(&wait_queue->lock);
    int len = list_length(&wait_queue->wait_list);
    xthread_spin_unlock(&wait_queue->lock);
    return len;
}

#ifdef __cplusplus
}
#endif

#endif   /* _XTHREAD_WAITQUEUE_H */
