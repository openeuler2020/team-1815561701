#ifndef _XTHREAD_TIMER_INNER_H
#define _XTHREAD_TIMER_INNER_H

#ifdef __cplusplus
extern "C" {
#endif

/* 30day = 30 * 60 * 60 * 24 * 1000 */
#define XTIMER_IDLE_TIMEOUT  (30 * 24 * 60 * 60  * 1000UL)

extern volatile int __xtimer_handle_failed;

void xthread_timer_update();
int xthread_timer_init();

#ifdef __cplusplus
}
#endif

#endif /* _XTHREAD_TIMER_INNER_H */