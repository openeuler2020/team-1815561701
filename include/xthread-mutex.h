#ifndef _XTHREAD_MUTEX_H
#define _XTHREAD_MUTEX_H

#include "xthread-spinlock.h"
#include "inneral/list.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    xthread_spin_lock_t wait_lock;
    list_t wait_list;
    int waiters;
} xthread_mutex_t;

static inline void xthread_mutex_init(xthread_mutex_t *mutex)
{
    xthread_spin_init(&mutex->wait_lock);
    list_init(&mutex->wait_list);
    mutex->waiters = 0;
}

void xthread_mutex_lock(xthread_mutex_t *mutex);
void xthread_mutex_unlock(xthread_mutex_t *mutex);

static inline int xthread_mutex_try_lock(xthread_mutex_t *mutex)
{
    return xthread_spin_trylock(&mutex->wait_lock);
}

static inline int xthread_mutex_is_locked(xthread_mutex_t *mutex)
{
    return xthread_spin_is_locked(&mutex->wait_lock);
}

#ifdef __cplusplus
}
#endif

#endif /* _XTHREAD_MUTEX_H */