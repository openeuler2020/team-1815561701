#ifndef _XTHREAD_H
#define _XTHREAD_H

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#define XTHREAD_VER_MASTER  1
#define XTHREAD_VER_SLAVE   0
#define XTHREAD_VER ((XTHREAD_VER_MASTER << 16) + XTHREAD_VER_SLAVE)

/* 线程栈参数 */
#define XTHREAD_STACKSIZE_MIN       (4 * 1024) /* 最小栈大小 */   
#define XTHREAD_STACKSIZE_DEL       XTHREAD_STACKSIZE_MIN

/* 启动状态 */
#define XTHREAD_CREATE_DETACHED       1 /* 分离状态启动 */
#define XTHREAD_CREATE_JOINABLE         0 /* 正常启动进程 */

/* 取消状态 */
#define XTHREAD_CANCEL_ENABLE       1   /* 收到信号后设置为CANCEL状态 */
#define XTHREAD_CANCEL_DISABLE      0   /* 收到信号忽略信号继续运行 */

/* 取消动作执行时机 */
#define XTHREAD_CANCEL_DEFFERED             2   /* 收到信号后继续运行至下一个取消点再退出 */
#define XTHREAD_CANCEL_ASYCHRONOUS  3   /* 立即执行取消动作（退出） */

typedef long xthread_t;

/* xthread attr struct */
typedef struct __xthread_attr {
    int detachstate;
    void *stackaddr;
    size_t stacksize;
} xthread_attr_t;

int xthread_create(xthread_t *tid, xthread_attr_t *attr, void * (*start_routine) (void *), void *arg);
void xthread_exit(void *retval);
int xthread_join(xthread_t tid, void **thread_return);
int xthread_detach(xthread_t tid);
xthread_t xthread_self(void);
int xthread_equal(xthread_t thread1, xthread_t thread2);
int xthread_cancel(xthread_t tid);
int xthread_setcancelstate(int state, int *oldstate);
int xthread_setcanceltype(int type, int *oldtype);
void xthread_testcancel(void);

/* xthread attr */
int xthread_attr_init(xthread_attr_t *attr);
int xthread_attr_destroy(xthread_attr_t *attr);
int xthread_attr_getdetachstate(const xthread_attr_t *attr, int *detachstate);
int xthread_attr_setdetachstate(xthread_attr_t *attr, int detachstate);    
int xthread_attr_getstacksize(const xthread_attr_t *attr, size_t *stacksize);
int xthread_attr_setstacksize(xthread_attr_t *attr, size_t stacksize);
int xthread_attr_getstackaddr(const xthread_attr_t *attr, void **stackaddr);
int xthread_attr_setstackaddr(xthread_attr_t *attr, void *stackaddr);

void xthread_yield();
int xthread_enable_interrupt();
int xthread_disable_interrupt();
int xthread_sleep(int seconds);
void xthread_usleep(int usec);
unsigned long xthread_best_count();

#ifdef __cplusplus
}
#endif

#include "xthread-spinlock.h"
#include "xthread-timer.h"
#include "xthread-mutex.h"
#include "xthread-sema.h"

#endif /* _XTHREAD_H */