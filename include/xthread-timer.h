#ifndef _XTHREAD_TIMER_H
#define _XTHREAD_TIMER_H

#include "inneral/list.h"
#include "xthread-spinlock.h"

#include <sys/types.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*xthread_timer_callback_t) (void *); 

/* 定时器 */
typedef struct {
    list_t list;                /* 定时器链表 */
    clock_t timeout;            /* 超时点，以ticks为单位 */
    void *arg;          /* 参数 */
    unsigned long id;           /* 定时器的id值 */
    xthread_timer_callback_t callback;  /* 回调函数 */
    xthread_spin_lock_t lock;
} xthread_timer_t;

static inline void xthread_timer_sethandler(xthread_timer_t *xtimer, xthread_timer_callback_t callback)
{
    xthread_spin_lock(&xtimer->lock);
    xtimer->callback = callback;
    xthread_spin_unlock(&xtimer->lock);
}

static inline void xthread_timer_setarg(xthread_timer_t *xtimer, void *arg)
{
    xthread_spin_lock(&xtimer->lock);
    xtimer->arg = arg;
    xthread_spin_unlock(&xtimer->lock);
}

xthread_timer_t *xthread_timer_create(unsigned long timeout, xthread_timer_callback_t callback, void *arg);
int xthread_timer_destroy(xthread_timer_t *timer);
void xthread_timer_set(xthread_timer_t *timer, unsigned long timeout, xthread_timer_callback_t callback, void *arg);
int xthread_timer_add(xthread_timer_t *timer);
void xthread_timer_del(xthread_timer_t *timer);
void xthread_timer_modify(xthread_timer_t *timer, unsigned long timeout);
int xthread_timer_cancel(xthread_timer_t *timer);
int xthread_timer_alive(xthread_timer_t *timer);

#ifdef __cplusplus
}
#endif

#endif /* _XTHREAD_TIMER_H */