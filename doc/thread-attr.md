# thread attr api

```c 
typedef struct __xthread_attr {
    int detachstate;
    void *stackaddr;
    size_t stacksize;
} xthread_attr_t;

detachstate：线程的分离状态，状态值如下
    XTHREAD_CREATE_DETACHED：分离状态启动，不能被其他线程join等待
    XTHREAD_CREATE_JOINABLE：正常启动，可以不被join
```

```c 
/**
 * 初始化属性为默认值，比如分配
 * @attr:   要初始化的属性指针
 * 
 * @return: 成功返回0,失败返回-1  
 */
int xthread_attr_init(xthread_attr_t *attr);
```

```c 
/**
 * 销毁一个属性，如果有线程栈，就会释放线程栈
 * @attr:   要销毁的属性指针
 * 
 * @return: 成功返回0,失败返回-1  
 */
int xthread_attr_destroy(xthread_attr_t *attr);
```

```c 
/**
 * 获取分离状态
 * @attr:   属性指针
 * @detachstate:    保存分离状态的指针
 * 
 * @return: 成功返回0,失败返回-1  
 */
int xthread_attr_getdetachstate(const xthread_attr_t *attr, int *detachstate);
```

```c 
/**
 * 设置分离状态
 * @attr:   属性指针
 * @detachstate:    分离状态
 * 
 * @return: 成功返回0,失败返回-1  
 */
int xthread_attr_setdetachstate(xthread_attr_t *attr, int detachstate);    
```

```c 
/**
 * 获取栈大小
 * @attr:   属性指针
 * @stacksize:    保存栈大小的指针
 * 
 * @return: 成功返回0,失败返回-1  
 */
int xthread_attr_getstacksize(const xthread_attr_t *attr, size_t *stacksize);
```

```c 
/**
 * 设置栈大小
 * @attr:   属性指针
 * @stacksize:    栈大小
 * 
 * @return: 成功返回0,失败返回-1  
 */
int xthread_attr_setstacksize(xthread_attr_t *attr, size_t stacksize);
```


```c 
/**
 * 获取栈地址
 * @attr:   属性指针
 * @stackaddr:    保存栈地址的指针
 * 
 * @return: 成功返回0,失败返回-1  
 */
int xthread_attr_getstackaddr(const xthread_attr_t *attr, void **stackaddr);
```

```c 
/**
 * 设置栈地址
 * @attr:   属性指针
 * @stackaddr:    栈地址
 * 
 * @return: 成功返回0,失败返回-1  
 */
int xthread_attr_setstackaddr(xthread_attr_t *attr, void *stackaddr);
```