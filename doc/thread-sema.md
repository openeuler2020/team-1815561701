# thread sema api

```c 
/**
 * 初始化信号量
 * @sema:   信号量指针
 * @value:  信号量的值
 */
void xthread_sema_init(xthread_sema_t *sema, int value);
```

```c 
/**
 * 销毁信号量
 * @sema:   信号量指针
 */
void xthread_sema_destroy(xthread_sema_t *sema);
```


```c 
/**
 * 等待信号量，等待成功，信号量-1
 * @sema:   信号量指针
 */
void xthread_sema_wait(xthread_sema_t *sema);
```

```c 
/**
 * 尝试等待信号量
 * @sema:   信号量指针
 * 
 * @return: 等待成功返回1,失败返回0
 */
int xthread_sema_try_wait(xthread_sema_t *sema);
```

```c 
/**
 * 在限定时间内，等待信号量
 * @sema:   信号量指针
 * @tv:     超时时间，绝对超时时间，也就是到达这个时刻后就返回
 * 
 * @return: 在限定时间内等待失败返回ETIMEDOUT，成功返回0
 */
int xthread_sema_wait_timeout(xthread_sema_t *sema, struct timeval *tv);
```


```c 
/**
 * 释放信号量
 * @sema:   信号量指针
 */
void xthread_sema_post(xthread_sema_t *sema);
```

```c 
/**
 * 获取信号量当前的值
 * @sema:   信号量指针
 * @sval:   保存信号量的指针
 * 
 * @return: 成功返回0,失败返回-1
 */
int xthread_sema_getvalue(xthread_sema_t *sema, int *sval);
```