# thread timer api

```c 
typedef void (*xthread_timer_callback_t) (void *); 

定时器回调函数：void func(void *arg);
```

```c 
/**
 * 创建一个定时器
 * @timeout:   定时器超时值（毫秒为单位）
 * @callback:   回调函数指针
 * @arg:        函数参数
 * 
 * @return: 成功返回定时器指针,失败返回NULL  
 */
xthread_timer_t *xthread_timer_create(unsigned long timeout, xthread_timer_callback_t callback, void *arg);
```

```c 
/**
 * 销毁一个定时器
 * @timer:   定时器
 * 
 * @return: 成功返回0,失败返回-1  
 */
int xthread_timer_destroy(xthread_timer_t *timer);
```

```c 
/**
 * 初始化定时器
 * @timer:   定时器
 * @timeout: 超时值（毫秒为单位）
 * @callback: 回调函数
 * @arg:    参数
 */
void xthread_timer_set(xthread_timer_t *timer, unsigned long timeout, xthread_timer_callback_t callback, void *arg);
```

```c 
/**
 * 添加定时器到系统，定时器生效
 * @timer:   定时器
 * 
 * @return: 成功返回0,失败返回-1  
 */
int xthread_timer_add(xthread_timer_t *timer);
```

```c 
/**
 * 从系统中删除定时器
 * @timer:   定时器
 */
void xthread_timer_del(xthread_timer_t *timer);
```

```c 
/**
 * 修改定时器超时
 * @timer:   定时器
 * @timeout: 超时值（毫秒）
 */
void xthread_timer_modify(xthread_timer_t *timer, unsigned long timeout);
```

```c 
/**
 * 取消一个正在执行中的定时器
 * @timer:   定时器
 * 
 * @return: 成功返回0,失败返回-1  
 */
int xthread_timer_cancel(xthread_timer_t *timer);
```

```c 
/**
 * 检测定时器是否在系统中
 * @timer:   定时器
 * 
 * @return: 在系统中返回1,不在返回0  
 */
int xthread_timer_alive(xthread_timer_t *timer);
```