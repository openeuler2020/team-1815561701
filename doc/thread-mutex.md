# thread mutex api

```c 
/**
 * 初始化互斥锁
 * @mutex:   锁指针
 */
void xthread_mutex_init(xthread_mutex_t *mutex);
```

```c 
/**
 * 互斥锁加锁
 * @mutex:   锁指针
 */
void xthread_mutex_lock(xthread_mutex_t *mutex);
```

```c 
/**
 * 互斥锁解锁
 * @mutex:   锁指针
 */
void xthread_mutex_unlock(xthread_mutex_t *mutex);
```

```c 
/**
 * 尝试互斥锁加锁
 * @mutex:   锁指针
 * 
 * @return: 加锁成功返回1,失败返回0
 */
int xthread_mutex_try_lock(xthread_mutex_t *mutex);
```

```c 
/**
 * 检测锁是否上锁
 * @mutex:   锁指针
 * 
 * @return: 已经上锁返回1,没有上锁返回0
 */
int xthread_mutex_is_locked(xthread_mutex_t *mutex);
```