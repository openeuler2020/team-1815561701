# thread spin lock api

```c 
/**
 * 初始化自旋转锁
 * @lock:   锁指针
 */
void xthread_spin_init(xthread_spin_lock_t* lock);
```

```c 
/**
 * 加锁，如果锁被占用，则循环等待
 * @lock:   锁指针
 */
void xthread_spin_lock(xthread_spin_lock_t* lock);
```

```c 
/**
 * 尝试加锁，如果锁被占用，则直接返回
 * @lock:   锁指针
 * 
 * @return: 加锁成功返回1,失败返回0
 */
int xthread_spin_trylock(xthread_spin_lock_t* lock);
```

```c 
/**
 * 解锁，释放对锁的占用
 * @lock:   锁指针
 */
void xthread_spin_unlock(xthread_spin_lock_t* lock);
```

```c 
/**
 * 查看锁是否已经加锁
 * @lock:   锁指针
 * 
 * @return: 已经加锁返回1,没有加锁返回0
 */
int xthread_spin_is_locked(xthread_spin_lock_t* lock)
```
