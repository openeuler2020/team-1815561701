# xthread开发文档
## 【xthread实现原理】
`xthread`基于`ucontext`和`signal`进行实现。ucontext拿来做任务切换，signal拿来模拟中断的产生。  
### 1.线程切换流程
线程切换的核心是调用`swapcontext(&prev->context, &next->context)`，会把当前的上下文环境保存到`prev`中，然后设置当前的执行环境为`next`的上下文中的内容。线程的切换也分为2种：a.主动切换：通过调用`xthread_yield`可以让出当前执行权，等待下一次被调度，然后执行后面的内容。b.被动切换：当产生中断（`signal SIGALARM`信号）时，对该信号进行捕捉，然后在信号处理中进行调度，就可以切换到其他任务。  
### 2.SIGALARM信号的使用
当第一个线程创建的时候，会通过`setitimer(ITIMER_REAL, &value, NULL)`设置一个定时器超时的值，当超时后，就会产生SIGALARM信号，此时，我们之前通过`signal(SIGALRM, xthread_alarm_handler)`注册了信号捕捉函数，就可以在信号产生的时候执行里面的内容。在`xthread_alarm_handler`函数中，调用`xthread_schedule()`就可以进行调度切换到下一个任务了。  

## 【xthread使用教程】
线程的学习主要包括四方面：线程管理（创建、分离、`joinable`以及设置线程属性等）；互斥锁（创建、销毁、`lock` 和`unlock`等）；信号量（创建、销毁、`wait` 和`post`等）; 定时器（创建、销毁、添加、取消、注册定时器函数）。  
### 1、线程创建
```c
int xthread_create(xthread_t *tid, xthread_attr_t *attr, void * (*start_routine) (void *), void *arg);
```
参数：
```c
tid:    存放线程的id，可以是NULL
attr:   线程的属性，NULL则用默认属性
start_rotine:   线程的入口
arg:    传递给线程的参数，可以NULL
```
线程创建时应保证线程`ID thread`被成功创建（检查返回值）；线程属性attr默认值为**NULL**，可设置为其他属性；`start_routine`是创建线程后所执行的函数体；`arg`是传入的参数。  
### 2、线程等待和终止
```c
void xthread_exit(void *retval);
int xthread_join(xthread_t tid, void **thread_return);
```
`xthread_exit()` 终止线程，并且提供指针变量\*`value_ptr`给`xthread_join()`调用, xthread_join() 阻塞调用线程调用，并等待线程结束，得到可选返回值\*`value_ptr`。
实例：

### 3、线程属性
属性的初始化、属性的设置`set`、属性的获取`get`
```c
int xthread_attr_init(xthread_attr_t *attr);
int xthread_attr_destroy(xthread_attr_t *attr);
int xthread_attr_getdetachstate(const xthread_attr_t *attr, int *detachstate);
int xthread_attr_setdetachstate(xthread_attr_t *attr, int detachstate);    
int xthread_attr_getstacksize(const xthread_attr_t *attr, size_t *stacksize);
int xthread_attr_setstacksize(xthread_attr_t *attr, size_t stacksize);
int xthread_attr_getstackaddr(const xthread_attr_t *attr, void **stackaddr);
int xthread_attr_setstackaddr(xthread_attr_t *attr, void *stackaddr);
```
示例：

```c

#define STACK_SIZE  1024*10

void *thread1(void *arg)
{
    printf("this is thread %d , say:%s.\n", xthread_self(), (char *)arg);
    printf("this is thread %d , I will sleep 3 s.\n", xthread_self());
    xthread_sleep(3);
    printf("this is thread %d , I will deatch.\n", xthread_self());
    xthread_detach(xthread_self());
    printf("thread %d: sleep done, exit right now\n", xthread_self());
    xthread_exit((void *) 1);
}

void *thread2(void *arg)
{
    printf("this is thread %d , say:%s.\n", xthread_self(), (char *)arg);
    
    int sleep_count = 3;
    while (sleep_count > 0) {
        xthread_usleep(1000 * 100);
        sleep_count--;
    }
    printf("thread %d: sleep done, exit right now\n", xthread_self());
    xthread_exit((void *) 1);
}

int test_thread(int argc, char *argv[])
{
    /* create a thread without attr */
    xthread_t tid0, tid1;
    if (xthread_create(&tid0, NULL, thread1, (void *) "hello, world!") < 0)
        printf("xthread create failed!\n");
    printf("create thread %d ok.\n", tid0);
    void *thread_stack = malloc(STACK_SIZE);
    assert(thread_stack);
    xthread_attr_t attr;
    xthread_attr_init(&attr); // init to default attr
    xthread_attr_setstackaddr(&attr, thread_stack);
    xthread_attr_setstacksize(&attr, STACK_SIZE);
    
    int state;
    xthread_attr_getdetachstate(&attr, &state);
    printf("xthread detach state %d\n", state);
    void *addr;
    xthread_attr_getstackaddr(&attr, &addr);
    printf("xthread stack addr %x\n", addr);
    size_t size;
    xthread_attr_getstacksize(&attr, &size);
    printf("xthread stack size %d\n", size);
    
    xthread_attr_setdetachstate(&attr, XTHREAD_CREATE_DETACHED);
    if (xthread_create(&tid1, &attr, thread2, (void *) "hello, world!") < 0)
        printf("xthread create failed!\n");
    printf("create thread %d ok.\n", tid1);
    
    void *status;
    xthread_join(tid1, &status);
    printf("thread join %d done with status %x.\n", tid1, status);
    xthread_join(tid0, &status);
    printf("thread join %d done with status %x.\n", tid0, status);
    printf("all thread exited, main sleep 1s.\n");
    
    xthread_sleep(1);
    return 0;
}
```

其实，这个代码和上一个代码是类似的：
线程一得等到`xthread_join`来释放系统资源，线程一的线程函数一结束就自动释放资源
总之为了在使用 xthread 时避免线程的资源在线程结束时不能得到正确释放，从而避免产生潜在的内存泄漏问题，在对待线程结束时，要确保该线程处于 `detached` 状态，否着就需要调用 xthread_join() 函数来对其进行资源回收。

### 4、线程互斥锁
当多个线程同时访问某个变量时，需要使用互斥锁来维持对该变量的访问与修改
```c
void xthread_mutex_init(xthread_mutex_t *mutex);    // 初始化互斥锁
void xthread_mutex_lock(xthread_mutex_t *mutex);    // 互斥锁加锁
void xthread_mutex_unlock(xthread_mutex_t *mutex);  // 互斥锁解锁
int xthread_mutex_try_lock(xthread_mutex_t *mutex); // 尝试对互斥锁加锁
int xthread_mutex_is_locked(xthread_mutex_t *mutex);    // 查看互斥锁是否已经锁住
```
示例：
```c

static int count = 0;

xthread_mutex_t mutex;

static void *thread_changer(void *arg)
{
    int old;
    while (1) {
        xthread_mutex_lock(&mutex);
        count++;
        old = count;
        printf("thread: %d, count=%d\n", xthread_self(), count);
        xthread_sleep(1);
        if (old != count)
            printf("thread: %d, count=%d -> old=%d are different!\n", xthread_self(), count, old);
        xthread_mutex_unlock(&mutex);
    }
}

static void *thread_reader(void *arg)
{
    while (1) {
        xthread_sleep(1);
        printf("thread: %d, count=%d\n", xthread_self(), count);
    }
}

int test_mutex(int argc, char *argv[])
{
    xthread_mutex_init(&mutex);
    count = 0;
    xthread_t tid0;
    if (xthread_create(&tid0, NULL, thread_changer, "a") < 0)
            printf("xthread create failed!\n");
    xthread_t tid1;
    if (xthread_create(&tid1, NULL, thread_reader, "b") < 0)
            printf("xthread create failed!\n");
    xthread_t tid2;
    if (xthread_create(&tid2, NULL, thread_changer, "c") < 0)
            printf("xthread create failed!\n");
    
    void *status;
    xthread_join(tid0, &status);
    printf("thread %x exit with %x\n", tid0, status);
    return 0;
}
```

### 5、信号量
```c
void xthread_sema_init(xthread_sema_t *sema, int value);    // 初始化信号量
void xthread_sema_destroy(xthread_sema_t *sema);    // 销毁信号量
int xthread_sema_wait_timeout(xthread_sema_t *sema, struct timeval *tv);    // 等待一个信号量超时
void xthread_sema_wait(xthread_sema_t *sema);   // 等待一个信号量
int xthread_sema_try_wait(xthread_sema_t *sema);    // 尝试等待一个信号量
void xthread_sema_post(xthread_sema_t *sema);   // 释放一个信号量
int xthread_sema_getvalue(xthread_sema_t *sema, int *sval); // 获取信号量当前的值

```
示例：
```c

static int count = 0;

xthread_sema_t sema;

static void *thread_changer(void *arg)
{
    int old;
    while (1) {
        xthread_sema_wait(&sema);
        count++;
        old = count;
        printf("thread: %d, count=%d\n", xthread_self(), count);
        if (old != count)
            printf("thread: %d, count=%d -> old=%d are different!\n", xthread_self(), count, old);
        xthread_sema_post(&sema);
    }
}

static void *thread_changer2(void *arg)
{
    int old;
    struct timeval tv;
    while (1) {
        gettimeofday(&tv, NULL);
        tv.tv_usec += 1000 * 200;
        if (xthread_sema_wait_timeout(&sema, &tv)) {
            printf("thread: %d, wait sema timeout\n", xthread_self());
            continue;
        }
        printf("thread: %d, wait sema no timeout\n", xthread_self());
            
        count++;
        old = count;
        printf("thread: %d, count=%d\n", xthread_self(), count);
        if (old != count)
            printf("thread: %d, count=%d -> old=%d are different!\n", xthread_self(), count, old);
        xthread_sema_post(&sema);
    }
}

static void *thread_reader(void *arg)
{
    while (1) {
        xthread_sema_wait(&sema);
        printf("thread: %d, count=%d\n", xthread_self(), count);
        xthread_sema_post(&sema);
        xthread_sleep(1);
    }
}

int test_sema(int argc, char *argv[])
{
    xthread_sema_init(&sema, 1);
    count = 0;
    xthread_t tid0;
    if (xthread_create(&tid0, NULL, thread_changer, "a") < 0)
        printf("xthread create failed!\n");
    
    printf("xthread create %d\n", tid0);
    xthread_t tid1;
    if (xthread_create(&tid1, NULL, thread_reader, "b") < 0)
        printf("xthread create failed!\n");
    printf("xthread create %d\n", tid1);
    xthread_t tid2;
    if (xthread_create(&tid2, NULL, thread_changer2, "c") < 0)
        printf("xthread create failed!\n");
    printf("xthread create %d\n", tid2);

    void *status;
    xthread_join(tid0, &status);
    printf("thread %x exit with %x\n", tid0, status);
    return 0;
}
```

### 6、定时器
由于`xthread`使用了底层的`settimer`功能，于是当需要使用定时器的时候，就可以使用`xthread_timer`。
```c
xthread_timer_t *xthread_timer_create(unsigned long timeout, xthread_timer_callback_t callback, void *arg); // 创建一个定时器，并传入超时值以及定时器回调函数
int xthread_timer_destroy(xthread_timer_t *timer);  // 销毁一个定时器，销毁前必须被删除
void xthread_timer_set(xthread_timer_t *timer, unsigned long timeout, xthread_timer_callback_t callback, void *arg);    // 重新设置定时器的值
int xthread_timer_add(xthread_timer_t *timer);  // 将定时器添加到系统中
void xthread_timer_del(xthread_timer_t *timer); // 删除定时器
void xthread_timer_modify(xthread_timer_t *timer, unsigned long timeout);   // 修改定时器
int xthread_timer_cancel(xthread_timer_t *timer);   // 取消一个定时器
int xthread_timer_alive(xthread_timer_t *timer);    // 查看定时器是否在系统中存活
```
示例：
```c

static void mytimer(void *arg)
{   
    xthread_timer_t *tmr = (xthread_timer_t *) arg;
    printf("5 s timer occur!\n");
    exit(0);
}

static void mytimer2(void *arg)
{
    printf("1 s timer occur!\n");
}

static void *timer_thread(void *arg)
{
    printf("timer thread starting...\n");
    printf("timer sleep 3 seconds.\n");
    xthread_sleep(3);
    printf("timer sleep 3 seconds done.\n");
    
    printf("set 1 seconds timeout timer.\n");
    xthread_timer_t *newtimer = xthread_timer_create(1000, mytimer2, NULL);
    xthread_timer_add(newtimer);
    
    printf("set 5 seconds timeout timer.\n");
    static xthread_timer_t timer;
    xthread_timer_set(&timer, 5000, mytimer, (void *) newtimer);
    xthread_timer_add(&timer);
    
    printf("timer thread end.\n");
    return (void *) 0x1234abcd;
}

int test_timer(int argc, char *argv[])
{
    xthread_t tid;
    if (xthread_create(&tid, NULL, timer_thread, NULL) < 0)
            printf("xthread create failed!\n");
    void *status;
    xthread_join(tid, &status);
    printf("thread %x exit with %x\n", tid, status);
    xthread_sleep(5);
    return 0;
}
```
完毕！