# thread api


```c 
typedef long xthread_t;
xthread_t是线程的结构，需要用它来保存线程
```

```c 
/**
 * 创建一个线程
 * @tid:    存放线程的id，可以是NULL
 * @attr:   线程的属性，NULL则用默认属性
 * @start_rotine:   线程的入口
 * @arg:    传递给线程的参数，可以NULL
 * 
 * @return: 成功返回0,失败返回错误码  
 */
int xthread_create(xthread_t *tid, xthread_attr_t *attr, void * (*start_routine) (void *), void *arg);
```

```c
/**
 * 退出一个线程
 * @retval: 返回值，如果有线程在等待，就传递给该线程
 */
void xthread_exit(void *retval);
```

```c
/**
 * 等待一个线程退出
 * @tid:    等待的线程id
 * @thread_return:  线程的返回值
 * 
 * @return: 成功返回0,失败返回-1 
 */
int xthread_join(xthread_t tid, void **thread_return);
```

```c
/**
 * 取消和等待线程的连接
 * @tid:    需要执行detach操作的线程
 * 
 * @return: 成功返回0,失败返回-1 
 */
int xthread_detach(xthread_t tid);
```


```c
/**
 * 获取当前线程的tid
 * 
 * @return: 当前线程的id
 */
xthread_t xthread_self(void);
```

```c
/**
 * 判断两个线程是否一样
 * @thread1: 线程1
 * @thread2: 线程2
 * 
 * @return: 一样返回1,不一样返回0
 */
int xthread_equal(xthread_t thread1, xthread_t thread2);
```

```c
/**
 * 取消一个正在执行中的线程
 * 如果线程有ASYCHRONOUS标志，则立即使该线程退出
 * @tid: 需要取消的线程
 * 
 * @return: 成功返回0,失败返回-1
 */
int xthread_cancel(xthread_t tid);
```

```c
/**
 * 设置取消状态
 * @state: 状态：XTHREAD_CANCEL_ENABLE允许被取消
 *              XTHREAD_CANCEL_DISABLE不允许被取消
 * @oldstate: 旧的状态
 * 
 * @return: 成功返回0,失败返回-1
 */
int xthread_setcancelstate(int state, int *oldstate);
```

```c
/**
 * 设置取消类型
 * @type: 类型：XTHREAD_CANCEL_DEFFERED：收到信号后继续运行至下一个取消点再退出
 *             XTHREAD_CANCEL_ASYCHRONOUS：立即执行取消动作（退出）
 * @oldtype: 旧的类型
 * 
 * @return: 成功返回0,失败返回-1
 */
int xthread_setcanceltype(int type, int *oldtype);
```

```c
/**
 * 测试取消点
 * 当收到cancel信号时，检测点需要在特定函数（xthread_sleep, xthread_usleep,xthread_yield, xthread_join,xthread_cancel,xthread_yield）调用时才会检测，于是在没有调用这些函数的地方，可以尝试调用该函数来主动检测取消点
 * 
 * @return: 成功返回0,失败返回-1
 */
void xthread_testcancel(void);
```

```c
/**
 * 暂时让出执行权
 */
void xthread_yield();
```

```c
/**
 * 休眠后在继续运行
 * @seconds:    休眠的时间数，秒为单位
 * 
 * @return: 返回剩余的时间数，比如被中途打断或者唤醒时
 */
int xthread_sleep(int seconds);
```

```c
/**
 * 休眠后在继续运行
 * @usec:    微妙数
 */
void xthread_usleep(int usec);
```

```c
/**
 * 打开“中断”
 * 打开中断后，才会产生中断，然后定期切换任务
 */
int xthread_enable_interrupt();
```

```c
/**
 * 关闭“中断”
 * 关闭中断后，就不会自动切换任务了。此时的xthread变得和coroutine一样了。
 */
int xthread_disable_interrupt();
```