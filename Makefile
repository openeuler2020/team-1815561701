MAKE = make

.PHONY: all clean run

all: 
	@$(MAKE) -s -C xthread
	@$(MAKE) -s -C demo
	
clean:
	@$(MAKE) -s -C xthread clean
	@$(MAKE) -s -C demo clean
	
run: clean all
	./demo/demo
